import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:realtime_chat/src/global/environment.dart';
import 'package:realtime_chat/src/models/mensajes_response.dart';
import 'package:realtime_chat/src/models/usuario_model.dart';
import 'package:realtime_chat/src/services/auth_service.dart';

//Este Provider necesita notificar a los hijos cuando algo cambie.
//por ello ira en el main, dentro de providers.
//Este ChatService tendra los mensajes relacionados con los usuarios.
class ChatService with ChangeNotifier {
//Este usuarioPara, es para quien van dirigidos los mensajes.
  Usuario usuarioPara;

//Creamos una peticion para recuperar todos los mensajes de chats.
  Future<List<Mensaje>> getChat(String usuarioID) async {
    final resp = await http.get('${Environment.apiUrl}/mensajes/$usuarioID',
    //final resp = await http.get('https://socket-server-chat-tabares.herokuapp.com/mensajes/$usuarioID',
        headers: {
          'Content-Type': 'application/json',
          'x-token': await AuthService.getToken()
        }
    );
    final mensajesResp = mensajesResponseFromJson(resp.body);
    
    return mensajesResp.mensajes;
  }
}
