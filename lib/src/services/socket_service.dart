import 'package:flutter/material.dart';
//import 'package:realtime_chat/src/global/environment.dart';
import 'package:realtime_chat/src/services/auth_service.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

//cuando inicie la aplicacion va ha estar en Connecting
enum ServerStatus { OnLine, OffLine, Connecting }

//Este ChangeNotifier  nos ayudara notificandole a provider cuando tiene
//que refrescar la IU o interfaz de usuario, o redibujar algun Widget en particular.
//o cuando quiera notificar a las personas que esten trabajando con este SocketServer.
class SocketService with ChangeNotifier {
  ServerStatus _serverStatus = ServerStatus.Connecting;
  IO.Socket _socket;

  ServerStatus get serverStatus => this._serverStatus;

//El socket escucha los eventos del server
  IO.Socket get socket => this._socket;
  Function get emit => this._socket.emit;

//cuando quiera conectarme al socket server llamo a conectar
  void conectarConSocketServer() async {
  
  //Al obtener el token debo conectarme de alguna manera, 
  //validando dicho token en el extraHeaders que recibe un mapa.
  final token = await AuthService.getToken();
  
  
// Dart client
    //this._socket = IO.io(Environment.socketUrl, {
    this._socket = IO.io('https://socket-server-chat-tabares.herokuapp.com/', {
      'transports': ['websocket'],
      'autoConnect': true,
      //forzamos una nueva conexion
      'forceNew': true,
      //validamos la conexion con 
      'extraHeaders':{
      'x-token': token
      }
    });

    this._socket.on('connect',(_) {
      print('connect');
      this._serverStatus = ServerStatus.OnLine;
      notifyListeners();
    });

    this._socket.on('disconnect',(_) {
      print('disconnect');
      this._serverStatus = ServerStatus.OffLine;
      notifyListeners();
    });
  }

  //Cuando hagamos el logOut invocamos a desconectar().
  void desconectarDeSocketServer() {
    this._socket.disconnect();
  }
}
