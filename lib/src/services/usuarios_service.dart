import 'package:http/http.dart' as http;

import 'package:realtime_chat/src/models/usuario_model.dart';
import 'package:realtime_chat/src/global/environment.dart';
import 'package:realtime_chat/src/models/usuarios_response.dart';

import 'package:realtime_chat/src/services/auth_service.dart';

class UsuariosService {


  Future<List<Usuario>> getUsuario() async {
    try {
//Realizamos la peticion http.
      final resp = await http.get('${Environment.apiUrl}/usuarios',
      //final resp = await http.get('https://socket-server-chat-tabares.herokuapp.com/usuarios',
      headers: {
        'Content-Type': 'application/json',
        'x-token': await AuthService.getToken()
      });
      //Ahora Mapeamos la  respuesta.
      final usuariosResponse = usuariosResponseFromJson(resp.body);
      //
      return usuariosResponse.usuarios;
      
    } catch (e) {
      return [];
    }
  }
}
