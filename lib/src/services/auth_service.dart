import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:realtime_chat/src/global/environment.dart';
import 'package:realtime_chat/src/models/login_response.dart';
import 'package:realtime_chat/src/models/usuario_model.dart';

class AuthService with ChangeNotifier {
//La idea de este servicio es que este usuario tenga la informacion del
//usuario actualmente logeado.
  Usuario usuario;

  bool _autenticando = false;
  // Create storage
  final _storage = new FlutterSecureStorage();

  bool get autenticando => this._autenticando;

  set autenticando(bool valor) {
    this._autenticando = valor;
    notifyListeners();
  }

  //Getters del token de forma statica.
  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token;
  }

  //Getters del token de forma statica.
  static Future<void> deleteToken() async {
    final _storage = new FlutterSecureStorage();
    await _storage.delete(key: 'token');
  }

//Hay que llamar este Future login en la pantalla de login.
  Future<bool> login(String email, String password) async {
    this.autenticando = true;
    final data = {'email': email, 'password': password};

    //anexo  al final /login para construirlo
    //final resp = await http.post('${Environment.apiUrl}/login' ,
    final resp = await http.post('${Environment.apiUrl}/login' ,
    //final resp = await http.post('https://socket-server-chat-tabares.herokuapp.com/login' ,
        body: jsonEncode(data), headers: {'Content-Type': 'application/json'});
    //La idea aqui es mapear la respuesta a un modelo propio de nuestra aplicacion en flutter
    //Lo podemos hacer mediante el sitio web
    print(resp.body);

    this.autenticando = false;

    if (resp.statusCode == 200) {
      //En el loginResponse almaceno toda la data relacionada a un usuario.
      final loginResponse = loginResponseFromJson(resp.body);
      //Cuando tenga una  autenticacion valida almacenara en el this.usuario un usuario.
      this.usuario = loginResponse.usuario;
      //asi ya lo tengo mapeado

      //Guardar token en lugar seguro.Espero a que se grabe con el await, y luego continua
      //ejecutando las demas lineas de codigo.
      await this._guardarToken(loginResponse.token);

      return true;
    } else {
      return false;
    }
  }

  Future register(String nombre, String email, String password) async {
    this.autenticando = true;

    final data = {'nombre': nombre, 'email': email, 'password': password};
    final resp = await http.post('${Environment.apiUrl}/login/new',
    //final resp = await http.post('https://socket-server-chat-tabares.herokuapp.com/login/new',
        body: jsonEncode(data), headers: {'Content-Type': 'application/json'});

    print(resp.body);
    this.autenticando = false;

    if (resp.statusCode == 200) {
      final loginResponse = loginResponseFromJson(resp.body);
      this.usuario = loginResponse.usuario;

      await this._guardarToken(loginResponse.token);

      return true;
    } else {
      //La siguiente linea es para maper de un jsonStrng a un mapa en dart.
      final respBody = jsonDecode(resp.body);
      //Ya mapeado retornamos el msg  con la definicion del mensaje de error.
      return respBody['msg'];
    }
  }

  Future<bool> isLoggedIn() async {
    final token = await this._storage.read(key: 'token');
    print(token);

    //Realizamos el llamado a nuestro endPoint. Debe de estar arriba el server, de lo contrario
    //arrojara error de conexion.
    final resp = await http.get('${Environment.apiUrl}/login/renew', headers: {
    //final resp = await http.get('https://socket-server-chat-tabares.herokuapp.com/login/renew', headers: {
      'Content-Type': 'application/json',
      //Anexo un header que fue el header personalizado  en su momento.
      'x-token': token
    });

    print(resp.body);

//Si la respuesta es correcta entonces sigue adelante
    if (resp.statusCode == 200) {
      final loginResponse = loginResponseFromJson(resp.body);
      this.usuario = loginResponse.usuario;
      //Guardamos el nuevo token
      await this._guardarToken(loginResponse.token);

      return true;
    } else {
    //En caso contrario ejecutamos logOut(), porque el token no sirve
      this.logOut();
      return false;
    }
  }

//Future encargado de guaradar el token creado en el login
  Future _guardarToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future logOut() async {
    // Delete value
    await _storage.delete(key: 'token');
  }
}
