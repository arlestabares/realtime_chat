import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:realtime_chat/src/helpers/mostrar_alerta.dart';
import 'package:realtime_chat/src/services/auth_service.dart';
import 'package:realtime_chat/src/services/socket_service.dart';
import 'package:realtime_chat/src/widgets/boton_login.dart';
import 'package:realtime_chat/src/widgets/custom_input.dart';
import 'package:realtime_chat/src/widgets/labels.dart';
import 'package:realtime_chat/src/widgets/logo.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffF2F2F2),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Logo(
                  titulo: 'Messenger',
                ),
                _Form(),
                Labels(
                    ruta: 'register',
                    mensaje: '¿No tienes Cuenta?',
                    mensajeDeCuenta: 'Crear Cuenta'),
                Text('Terminos y condiciones de uso',
                    style: TextStyle(fontWeight: FontWeight.w200)),
              ],
            ),
          ),
        ));
  }
}

class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final emailController = TextEditingController();
  final passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
  
    final authService   = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketService>(context);
    
    return Container(
      margin: EdgeInsets.only(top: 40),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: <Widget>[
          CustomInputTextField(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textControler: emailController,
          ),
          CustomInputTextField(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            textControler: passController,
            isPassword: true,
          ),
          BotonLogin(
              text: 'Ingrese',
              onPressed: authService.autenticando
                  ? null
                  : () async {
                      //Ocultamos el teclado
                      FocusScope.of(context).unfocus();
                      final loginOk = await authService.login(
                          emailController.text.trim(),
                          passController.text.trim());

                      if (loginOk) {
                        socketService.conectarConSocketServer();

                        // navegar a otra pantalla
                        Navigator.pushReplacementNamed(context, 'usuarios');
                      } else {
                        //mostrar alerta
                        mostrarAlerta(context, 'Login Incorrecto',
                            'Verifique sus credenciales');
                      }
                      // print(emailController.text);
                      //print(passController.text);
                    }),
        ],
      ),
    );
  }
}
