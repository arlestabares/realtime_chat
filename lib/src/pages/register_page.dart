import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:realtime_chat/src/helpers/mostrar_alerta.dart';

import 'package:realtime_chat/src/services/auth_service.dart';
import 'package:realtime_chat/src/services/socket_service.dart';
import 'package:realtime_chat/src/widgets/boton_login.dart';
import 'package:realtime_chat/src/widgets/custom_input.dart';
import 'package:realtime_chat/src/widgets/labels.dart';
import 'package:realtime_chat/src/widgets/logo.dart';

//Clase que sera tratada como un preloading, para guardar
//sera como un contenedor seguro
class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffF2F2F2),
        body: SafeArea(
          child: SingleChildScrollView(
                      child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Logo(titulo: 'Registro'),
                _Form(),
                Labels(
                  ruta: 'login',
                  mensaje: '¿Tienes cuenta?',
                  mensajeDeCuenta: 'Ingresa con tu cuenta',
                ),
                Text('Terminos y condiciones de uso',
                    style: TextStyle(fontWeight: FontWeight.w200)),
              ],
            ),
          ),
        ));
  }
}

class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final nameController  = TextEditingController();
  final emailController = TextEditingController();
  final passController  = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketService>(context);

    return Container(
      margin: EdgeInsets.only(top: 40),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: <Widget>[
          CustomInputTextField(
            icon: Icons.perm_identity,
            placeholder: 'Nombre',
            keyboardType: TextInputType.emailAddress,
            textControler: nameController,
          ),
          CustomInputTextField(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textControler: emailController,
          ),
          CustomInputTextField(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            textControler: passController,
            isPassword: true,
          ),
          BotonLogin(
              text: 'Crear Cuenta',
              onPressed: authService.autenticando
                  ? null
                  : () async {
                      print(nameController.text);
                      print(emailController.text);
                      print(passController.text);
                      
                      final registerOk = await authService.register(
                          nameController.text.trim(),
                          emailController.text.trim(),
                          passController.text.trim()
                     );

                      if (registerOk == true) {
                      socketService.conectarConSocketServer();
                        //navegar a otra pagina
                        Navigator.pushReplacementNamed(context, 'usuarios');
                      } else {
                      //Mostrar alerta
                      mostrarAlerta(context,'Registro incorrecto' ,registerOk);
                      }
                    }),
        ],
      ),
    );
  }
}
