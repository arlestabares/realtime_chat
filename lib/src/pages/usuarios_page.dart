import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:realtime_chat/src/models/usuario_model.dart';
import 'package:realtime_chat/src/services/auth_service.dart';
import 'package:realtime_chat/src/services/chat_service.dart';
import 'package:realtime_chat/src/services/socket_service.dart';
import 'package:realtime_chat/src/services/usuarios_service.dart';

class UsuariosPage extends StatefulWidget {
  @override
  _UsuariosPageState createState() => _UsuariosPageState();
}

class _UsuariosPageState extends State<UsuariosPage> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  final usuarioService = new UsuariosService();

  List<Usuario> dbUsuarios = [];

  // final usuarios = [
  //   Usuario( uid: '1',nombre: 'Roko',email: 'rokotabares@gmail.com', online: true),
  //   Usuario( uid: '2',nombre: 'Lola',email: 'lolatabares@gmail.com',online: false),
  //   Usuario(uid: '3',nombre: 'Kamar',email: 'kamartabares@gmaiil.com',online: true),
  //   Usuario(uid: '4',nombre: 'muñeca',email: 'munecatabares@gmail.com', online: false),
  //   Usuario(uid: '5',nombre: 'Tiara', email: 'tiaratabares@gmail.com', online: true),
  //   Usuario(uid: '6', nombre: 'princesa', email: 'princesatabares@gmail.com',online: true),
  // ];
  
  @override
  void initState() {
    this._cargarUsuarios();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketService>(context);

    final usuario = authService.usuario;

    return Scaffold(
        appBar: AppBar(
          title: Text(
            usuario.nombre,
            style: TextStyle(color: Colors.black54),
          ),
          elevation: 1,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.exit_to_app,
              color: Colors.black54,
            ),
            onPressed: () {
              //Desconectar el Socket server.
              socketService.desconectarDeSocketServer();

              //Para no llamar al provider, puedo utilizar directamente el AuthService
              //con sus metodos estaticos.
              Navigator.pushReplacementNamed(context, 'login');
              AuthService.deleteToken();
            },
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              child: (socketService.serverStatus == ServerStatus.OnLine)
                  ? Icon(Icons.check_circle, color: Colors.blue[400])
                  : Icon(Icons.offline_bolt, color: Colors.red),
            )
          ],
        ),
        body: SmartRefresher(
          controller: _refreshController,
          enablePullDown: true,
          //Paso solo la referencia del metodo al onRefresh
          onRefresh: _cargarUsuarios,
          header: WaterDropHeader(
            waterDropColor: Colors.blue[400],
            complete: Icon(Icons.check, color: Colors.blue[400]),
          ),
          child: _listViewUsuarios(),
        ));
  }

  ListView _listViewUsuarios() {
    return ListView.separated(
        physics: BouncingScrollPhysics(),
        itemBuilder: (_, i) => _usuariosListTile(dbUsuarios[i]),
        separatorBuilder: (_, i) => Divider(),
        itemCount: dbUsuarios.length);
  }

  ListTile _usuariosListTile(Usuario usuario) {
    return ListTile(
      title: Text(usuario.nombre),
      subtitle: Text(usuario.email),
      leading: CircleAvatar(
        child: Text(usuario.nombre.substring(0, 2)),
        backgroundColor: Colors.blue[100],
      ),
      trailing: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          color: usuario.online ? Colors.green[300] : Colors.red,
          borderRadius: BorderRadius.circular(100),
        ),
      ),
      onTap: (){
      print(usuario.nombre);
      print(usuario.email);
      print(usuario.uid);
      
      final chatService = Provider.of<ChatService>(context,listen: false);
      chatService.usuarioPara = usuario;
      //Ahora con el usuario seleccionado para el chat, navego hacia la ruta.
      Navigator.pushNamed(context, 'chat');
      
      },
    );
  }

  _cargarUsuarios() async {
  
    this.dbUsuarios = await usuarioService.getUsuario();

    setState(() {});

    // monitor network fetch
    // await Future.delayed(Duration(milliseconds: 500));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
}
