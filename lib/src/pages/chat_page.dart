import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:realtime_chat/src/models/mensajes_response.dart';

import 'package:realtime_chat/src/services/chat_service.dart';
import 'package:realtime_chat/src/services/socket_service.dart';
import 'package:realtime_chat/src/services/auth_service.dart';
import 'package:realtime_chat/src/widgets/chat_message.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

//Si queremos mostrar animaciones tenemos que decirle al State que debe sincronizarse
//con el vertical sink,es decir, que cada uno de los cuadros este sincronizado con
//y para eso tengo que mezclar este estado con el TickerProviderStateMixin
class _ChatPageState extends State<ChatPage> with TickerProviderStateMixin {
  ChatService chatService;
  AuthService authService;
  SocketService socketService; //dispone del emit().
  List<ChatMessage> _messages = [];
  final _textController = new TextEditingController();
  final _focusNode = new FocusNode();
  bool _estaEscribiendo = false;

  @override
  void initState() {
    super.initState();

    this.chatService = Provider.of<ChatService>(context, listen: false);
    this.socketService = Provider.of<SocketService>(context, listen: false);
    this.authService = Provider.of<AuthService>(context, listen: false);

    this.socketService.socket.on('mensaje-personal', _escucharMensaje);

    _cargarHistorialMensajes(this.chatService.usuarioPara.uid);
  }

//Cargamos el historial de mmensajes de los chats.
  void _cargarHistorialMensajes(String usuarioID) async {
  
    List<Mensaje> chat = await this.chatService.getChat(usuarioID);

    print(chat);

    //
    final messageHistory = chat.map((m) => new ChatMessage(
          texto: m.mensaje,
          uid: m.de,
          animationController: new AnimationController(
          //Con el ..forward() inmediatamente lanza la animacion
              vsync: this, duration: Duration(milliseconds:0))..forward(),
        ));
        
        setState(() {
          _messages.insertAll(0, messageHistory);
        });
  }

//
  void _escucharMensaje(dynamic payload) {
    //print('tengo mensaje $payload');

    ChatMessage message = new ChatMessage(
        texto: payload['mensaje'],
        uid: payload['de'],
        animationController: AnimationController(
            vsync: this, duration: Duration(milliseconds: 300)));
    setState(() {
      _messages.insert(0, message);
    });
    message.animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    final usuarioPara = chatService.usuarioPara;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Column(
          children: <Widget>[
            CircleAvatar(
              child: Text(
                usuarioPara.nombre.substring(0, 2),
                style: TextStyle(fontSize: 12),
              ),
              backgroundColor: Colors.blue[100],
              maxRadius: 16,
            ),
            //SizedBox(height:1),
            Text(
              usuarioPara.nombre,
              style: TextStyle(color: Colors.black54),
            ),
          ],
        ),
        centerTitle: true,
        elevation: 1,
      ),
      body: Container(
          child: Column(
        children: <Widget>[
          Flexible(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: _messages.length,
              itemBuilder: (_, i) => _messages[i],
              reverse: true,
            ),
          ),
          Divider(height: 5),
          Container(
            color: Colors.white,
            height: 150,
            child: _inputChat(),
          )
        ],
      )),
    );
  }

//Widget encargado de la creacion y configuracion de la caja de texto o TextField
//donde el usurio escribira y enviara los mensajes
  Widget _inputChat() {
    return SafeArea(
      child: Container(
        child: Row(
          children: <Widget>[
            Flexible(
              child: TextField(
                controller: _textController,
                onSubmitted: _handleSubmit,
                focusNode: _focusNode,
                onChanged: (texto) {
                  //cuando hay un valor en el TextField para postear

                  setState(() {
                    if (texto.trim().length > 0) {
                      _estaEscribiendo = true;
                    } else {
                      _estaEscribiendo = false;
                    }
                  });
                },
                decoration: InputDecoration(hintText: 'Enviar mensaje'),
              ),
            ),
            // Boton de enviar
            //verificamos la plataforma para mostrar los widgets con un ternario
            Container(
              child: !Platform.isIOS
                  ? CupertinoButton(child: Text('Enviar'), onPressed: () {})
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 4.0),
                      child: IconTheme(
                        data: IconThemeData(color: Colors.blue[400]),
                        child: IconButton(
                            icon: Icon(
                              Icons.send,
                            ),
                            onPressed: _estaEscribiendo
                                ? () =>
                                    _handleSubmit(_textController.text.trim())
                                : null),
                      ),
                    ),
            )
          ],
        ),
      ),
    );
  }

  //Funcion encargada de manipular el texto o el mensaje de texto
  _handleSubmit(String texto) {
    if (texto.length == 0) return;

    _textController.clear();
    _focusNode.requestFocus();
    print(texto);

    final newMessage = new ChatMessage(
      uid: authService.usuario.uid,
      texto: texto,
      animationController: AnimationController(
          vsync: this, duration: Duration(milliseconds: 200)),
    );

    _messages.insert(0, newMessage);
    newMessage.animationController.forward();

    setState(() {
      _estaEscribiendo = false;
    });

    //Este mensaje es enviado al servidor, para que lo escúche debe recibir
    //la signatura del emit('mensaje-personal')
    this.socketService.emit('mensaje-personal', {
      'de': this.authService.usuario.uid,
      'para': this.chatService.usuarioPara.uid,
      'mensaje': texto
    });
  }

  @override
  void dispose() {
    //Off del socket

    for (ChatMessage message in _messages) {
      message.animationController.dispose();
    }
    this.socketService.socket.off('mensaje-personal', _escucharMensaje);

    super.dispose();
  }
}
