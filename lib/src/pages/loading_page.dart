import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:realtime_chat/src/pages/login_page.dart';
import 'package:realtime_chat/src/pages/usuarios_page.dart';

import 'package:realtime_chat/src/services/auth_service.dart';
import 'package:realtime_chat/src/services/socket_service.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: checkLoginState(context),
        builder: (context, snapshot) {
          return Center(
            child: Text('LoginPage....autenticando'),
          );
        },
      ),
    );
  }

//promesa que no redibuja nada,solo chequea el estado del login actual
  Future checkLoginState(BuildContext context) async {
    final authService = Provider.of<AuthService>(context, listen: false);
    final socketService = Provider.of<SocketService>(context, listen: false);

//Nota: dependiendo de la siguiente respuesta navego hacia una pantalla especifica
    final autenticado = await authService.isLoggedIn();

    if (autenticado) {
      //Conectar al socket server.
      socketService.conectarConSocketServer();

      //  Navigator.pushReplacementNamed(context, 'usuarios');

      //Con el PageRouteBuider creamos una entrada diferente de la que nos brinda
      //pushReplacementNamed.
      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
              pageBuilder: (_, __, ___) => UsuariosPage(),
              transitionDuration: Duration(milliseconds: 0)));
    } else {
      // Navigator.pushReplacementNamed(context, 'login');

      Navigator.pushReplacement(
          context,
          PageRouteBuilder(
              pageBuilder: (_, __, ___) => LoginPage(),
              transitionDuration: Duration(milliseconds: 0)));
    }
  }
}
