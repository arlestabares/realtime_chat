import 'package:flutter/material.dart';

//Crea los labels de cuenta y crear cuenta
class Labels extends StatelessWidget {

final String ruta;
final String mensaje;
final String mensajeDeCuenta;


  const Labels({
  Key key, 
  @required this.ruta, 
  @required this.mensaje, 
  @required this.mensajeDeCuenta}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Text(this.mensaje,
            style: TextStyle(
                color: Colors.black54,
                fontSize: 15,
                fontWeight: FontWeight.w300)),
        SizedBox(height: 10),
        GestureDetector(
          child: Text(
            this.mensajeDeCuenta,
            style: TextStyle(
                color: Colors.blue[600],
                fontSize: 18,
                fontWeight: FontWeight.bold),
          ),
          onTap: (){
         Navigator.pushReplacementNamed(context, this.ruta);
          },
        ),
      ],
    ));
  }
}
