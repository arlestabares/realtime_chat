import 'package:flutter/material.dart';

class CustomInputTextField extends StatelessWidget {
  final IconData icon;
  final String placeholder;
  final TextEditingController textControler;
  final TextInputType keyboardType;
  final bool isPassword;

  const CustomInputTextField(
      {Key key,
      @required this.icon,
      @required this.placeholder,
      @required this.textControler,
      this.keyboardType = TextInputType.text,
      this.isPassword = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      padding: EdgeInsets.only(top: 5, left: 5, bottom: 5, right: 20),
      
      child: TextField(
        controller: this.textControler,
        //autocorrect: false,
        keyboardType: this.keyboardType,
        //no permite ver los caracteres escritos 
        obscureText: this.isPassword,
        //decoracion de la parte internaa del textfield
        decoration: InputDecoration(
            prefixIcon: Icon(this.icon),
            //focusedBorder quita la linea horizontal al textFiel
            focusedBorder: InputBorder.none,
            border: InputBorder.none,
            hintText: this.placeholder),
      ),
      
      //Decoracion del TextField
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black.withOpacity(0.05),
                offset: Offset(0, 5),
                blurRadius: 5)
          ]),
    );
  }
}
