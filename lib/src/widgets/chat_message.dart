import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:realtime_chat/src/services/auth_service.dart';

class ChatMessage extends StatelessWidget {
  final AnimationController animationController;
  final String texto;
  final String uid;

  const ChatMessage({Key key, 
  @required this.texto, 
  @required this.uid, 
  @required this.animationController
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
  
  final authService = Provider.of<AuthService>(context,listen: false);
  
    return FadeTransition(
    opacity: animationController,
      child: SizeTransition(
      sizeFactor: CurvedAnimation(parent: animationController,curve: Curves.easeOut),
              child: Container(
          // margin: EdgeInsets.all(8),
          child: this.uid == authService.usuario.uid ? _myMessage() : _notMyMessage(),
        ),
      ),
    );
  }

  Widget _myMessage() {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        padding: EdgeInsets.all(9.0),
        margin: EdgeInsets.only(bottom: 5, left: 50, right: 5),
        child: Text(
          this.texto,
          style: TextStyle(color: Colors.white),
        ),
        decoration: BoxDecoration(
          color: Color(0xff4D9Ef6),
          borderRadius: BorderRadius.circular(100),
        ),
      ),
    );
  }

  Widget _notMyMessage() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.all(9.0),
        margin: EdgeInsets.only(bottom: 5, left: 5, right: 50),
        child: Text(
          this.texto,
          style: TextStyle(color: Colors.black87),
        ),
        decoration: BoxDecoration(
          color: Color(0xffE4E5EB),
          borderRadius: BorderRadius.circular(100),
        ),
      ),
    );
  }
}
