import 'dart:io';

//Clase que me permite definir variables de entorno estaticas
class Environment {
  static String apiUrl = Platform.isAndroid
    //? 'http://10.0.2.2:3000/api'
      ? 'https://socket-server-chat-tabares.herokuapp.com/api'
    //: 'http://localhost:3000/api';
      : 'https://socket-server-chat-tabares.herokuapp.com/api';
    
  static String socketUrl =
     //  Platform.isAndroid ? 'http://10.0.2.2:3000' : 'http://localhost:3000';
       Platform.isAndroid ? 'https://socket-server-chat-tabares.herokuapp.com/' : 'https://socket-server-chat-tabares.herokuapp.com/';
     // Platform.isAndroid ? 'http://localhost:3000' : 'http://localhost:3000';
}