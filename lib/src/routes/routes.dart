import 'package:flutter/material.dart';
import 'package:realtime_chat/src/pages/chat_page.dart';
import 'package:realtime_chat/src/pages/loading_page.dart';
import 'package:realtime_chat/src/pages/login_page.dart';
import 'package:realtime_chat/src/pages/register_page.dart';
import 'package:realtime_chat/src/pages/usuarios_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'loading': (_) => LoadingPage(),
  'usuarios': (_) => UsuariosPage(),
  'chat': (_) => ChatPage(),
  'login': (_) => LoginPage(),
  'register': (_) => RegisterPage(),
};
